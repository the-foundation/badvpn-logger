#!/bin/bash
_ipforensic_quick_onelinewhob() {  echo $1|torify whob -npg $1 |sed 's/.\+ Searching...//g;s/\r/\n/' >/dev/shm/.cache.whob_$1 ; };
_ipforensic_quick_revptr()      {  revinfo=$(host -t ptr $1 |grep "pointer"|sed 's/.\+ pointer //g');
                                   [[ "${DEBUGME}" = "true" ]] && echo hostin'|'"$1"'|rev:'$revinfo >&2
                                echo $revinfo ; } ;

#/bin/bash -c "torify whob -gnp 1.2.3.4"|sed 's/.\+Searching...//g;s/\r/\n/g'|sed 's/^'${targetv4}'/\0 = '${revinfo}'/g'
#flush buffer each line
stdbuf -oL badvpn-tun2socks $@ | while read logline;do
  stamp=$(date +%F_%T)
  [[ $logline =~ (accepted|up|closed|sent to client)$ ]] && echo -n #{ echo -n ${logline} ; } ;
  [[ $logline =~ (accepted)$ ]] && {
    reason=$BASH_REMATCH ;
    ip=${logline//*\(tun2socks\)*\(/};
    ip=${ip// */};
    port=${ip/*:/} ;
    ip=${ip/:*/} ;
#    echo $ip @ $port >/dev/shm/.currtun2socksip ;
    revinfo=$(_ipforensic_quick_revptr $ip);
#    find /dev/shm/.cache_whob -mtime +1 -delete
    test -e /dev/shm/.cache.whob_$ip   ||   _ipforensic_quick_onelinewhob $ip
                 regioinfo=$(grep -e Country-Code  -e Region -e City      /dev/shm/.cache.whob_$ip |cut -d":" -f2| sed 's/$/|/g;s/\t/ /g;s/ \+/ /g;s/|/ → /g'|tr -d '\n');
                  netnames=$(grep -e AS-Org-Name -e Org-Name -e Net-Name  /dev/shm/.cache.whob_$ip |cut -d":" -f2| awk '!x[$0]++'|sed 's/$/←/g;s/\t/ /g;s/ / /g;s/The entire subnet is used for shared hosting by customers of/CUSTOMER_SUBNET:/g'|tr -d '\n')
     asinfo=$(printf "%8s" $(grep -e Origin-AS /dev/shm/.cache.whob_$ip|sed 's/Origin-AS://g;s/ / /g;'|tr -d '\n' ) );
                      cidr=$(grep   -e Prefix  /dev/shm/.cache.whob_$ip|sed 's/Prefix://g;s/ \+/ /g;'     |tr -d '\n' );
     printf "| $stamp | %16s %6s | AS: %8s  | %20s | %48s\t| %44s | %16s " "${ip}" "${port}" "${asinfo//  / }" "${cidr//  / }" "${netnames//  /  }" "${regioinfo//  / }" "PTR: ${revinfo}" ;echo -n " |:: ${logline//INFO(tun2socks): /} ";

     echo   ; } ;done

