# Badvpn Logger

recommended use:
* add routing table (and add the default rout as pointed out in badvpn readme)
* `ip rule from 12.34.56.12 to 0.0.0.0 dport 443 lookup tablename`

* connect your client ( or use load balancers with checks ):
  ` while true;do sleep 4;ssh -o ServerAliveCountMax=2,reconnect myuser@myremoteserver.lan -D 19999 ./badvpn-udpgw --listen-addr 127.0.0.1:7302 --logger stdout --loglevel info;done`
* in another terminal: run badvpn-logger like this:
  ` bash badvpn-logger.sh --tundev tun0 --netif-ipaddr 10.0.0.2 --netif-netmask 255.255.255.0 --socks-server-addr 127.0.0.1:19999 --udpgw-remote-server-addr 127.0.0.1:7302 2>&1`

![screenie.png](./screenie.png)
